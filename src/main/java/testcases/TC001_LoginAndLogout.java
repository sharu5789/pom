package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_LoginAndLogout extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName="TC001_LoginAndLogout";
		testDescription="Login into leaftaps";
		authors="Gayatri";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="TC002";
		
	}
	@Test(dataProvider="fetchData")
	public void loginAndLogout(String uname, String pwd, String cname, String fname, String lname,String source,String mc, String personal, String profile, String rev, String industry, String owner, String sic, 
			String dec, String note, String localname, String dep, String employee, String ticket, 
			String country, String area, String ext, String email, String phonenum, String ask, String url ) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.crmsfa()
		.clickLead()
		.clickCreateLead()
		.typeCompanyName(cname)
		.typeFirstname(fname)
		.typeLastname(lname)
		.sourcedropdown(Integer.parseInt(source))

		.clickSelect(Integer.parseInt(mc))
				.typeper(personal)
				.typeprof(profile)
				.typeannual(rev)
				.industry(industry)
				.owner(owner)
				.typesic(sic)
				.typedes(dec)
				.typenote(note)
				.typelocal(localname)
				.typedepartment(dep)
				.typeemp(employee)
				.typeticket(ticket)
				.typecoountry(country)
				.typearea(area)
				.typeext(ext)
				.typeemail(email)
				.typenum(phonenum)
				.typeask(ask)
				.typeurl(url)
				.clickcreate();

	}
}
