package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_MergeLeads extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName="TC002_MergeLeads";
		testDescription="Login into leaftaps";
		authors="Sharanya";
		category="Smoke";
		testNodes ="MergeLeads";
		dataSheetName="TC003";
	}
	@Test(dataProvider="fetchData")
	public void MergeLeads(String uname, String pwd, String fname, String fname1) throws InterruptedException {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.crmsfa()
		.clickLead()
		.ClickMergeLead()
		.clickFromLead()
		.typeFirstName(fname)
		.clickLeadButton()
		.clickFirstLeadId()
	    .clickToLead()
	    .typeFirstName1(fname1)
	    .clickLeadButton1()
	    .clickFirstLeadId()
	    .clickMerge();
	}
}
