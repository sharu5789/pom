package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeFindLeads1 extends ProjectMethods {
	 public MergeFindLeads1()

	{
		PageFactory.initElements(driver, this);
	}

	
	@FindBy(xpath =  "(//input[@type='text'])[2]")
	private WebElement eleFN1;
	public MergeFindLeads1 typeFirstName1(String data) {
		type(eleFN1, data);
		return this;
	}
	
	@FindBy(xpath="//button[contains(text(),'Find Leads')]")
	private WebElement eleClickLead1;
	public MergeFindLeads1 clickLeadButton1() {
		
		click(eleClickLead1);
				 
		return this;
		
		
		
		
		}
	@FindBy(xpath= "(//div[contains(@class,'x-grid3-cell-inner x-grid3-col-partyId')])[1]/a")
	private WebElement eleClickFirstLeadId;
	public MergeLead clickFirstLeadId() throws InterruptedException {
		Thread.sleep(1000);
		click(eleClickFirstLeadId);
		switchToWindow(0);
		
		 
		return new MergeLead();
		
	}
}
