package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLead extends ProjectMethods{ 
	public MergeLead()

	{
		PageFactory.initElements(driver, this);
	}


	@FindBy(xpath="//img[@alt='Lookup']")
	private WebElement eleClickFromLead;
	public MergeFindLeads clickFromLead() {
		click(eleClickFromLead);
		switchToWindow(1);
		return new MergeFindLeads();


	}



	@FindBy(xpath="(//img[@alt='Lookup'])[2]")
	private WebElement eleClickToLead;
	public MergeFindLeads1 clickToLead() {
		click(eleClickToLead);
		switchToWindow(1);
		return new MergeFindLeads1();




	}
	@FindBy(xpath="//a[contains(@class,'buttonDangerous')]")
	private WebElement eleClickMerge;
	public MergeLead clickMerge() {
		//WebElement eleUsername = locateElement("id", "username");
		;
		//LoginPage lp = new LoginPage();
		click(eleClickMerge);

		acceptAlert();
		return this;
	}}
