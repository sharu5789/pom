package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Leads extends ProjectMethods{

	public Leads() {
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(linkText ="Leads")
	private WebElement eleLead;
	public MyLeads clickLead() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLead);
		return new MyLeads();
	
	
	
}
	
	
	
	
	
	
}
	
	



