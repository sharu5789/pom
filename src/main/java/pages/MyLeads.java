package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeads extends ProjectMethods {


	public MyLeads()

	{
		PageFactory.initElements(driver, this);
	}


	@FindBy(linkText ="Create Lead")
	private WebElement eleCreateLead;
	public CreateLead clickCreateLead() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleCreateLead);
		return new CreateLead();
	}

	@FindBy(linkText ="Merge Leads")
	private WebElement eleMergeLead;
	public MergeLead ClickMergeLead() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleMergeLead);
		return new MergeLead();
	}
}