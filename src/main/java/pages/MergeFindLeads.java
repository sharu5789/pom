package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeFindLeads extends ProjectMethods {

	public MergeFindLeads() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath = "//input[@name='firstName']")
	private WebElement eleFN;
	public MergeFindLeads typeFirstName(String data) {
		type(eleFN, data);
		return this;
	}
	
	@FindBy(xpath="//button[contains(text(),'Find Leads')]")
	private WebElement eleClickLead;
	public MergeFindLeads clickLeadButton() {
		
		click(eleClickLead);
		return this;
		
		
		
		
		}
	@FindBy(xpath= "(//div[contains(@class,'x-grid3-cell-inner x-grid3-col-partyId')])[1]/a")
	private WebElement eleClickFirstLeadId;
	public MergeLead clickFirstLeadId() throws InterruptedException {
		Thread.sleep(1000);
		click(eleClickFirstLeadId);
		switchToWindow(0);
		
		 
		return new MergeLead();
		
	}
	
}