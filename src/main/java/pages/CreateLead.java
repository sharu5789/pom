package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {
	
	public CreateLead()
	
	 {
			PageFactory.initElements(driver, this);
		}
		
	@FindBy(id="createLeadForm_companyName")
	private WebElement eleCompanyname;
	public CreateLead typeCompanyName(String data) {
		//WebElement eleUsername = locateElement("id", "username");
		type(eleCompanyname, data);
		//LoginPage lp = new LoginPage();
		return this;
	
		
		
		
		
	}

	@FindBy(id="createLeadForm_firstName")
	private WebElement elefirstName;
	public CreateLead typeFirstname(String data) {
		//WebElement eleUsername = locateElement("id", "username");
		type(elefirstName, data);
		//LoginPage lp = new LoginPage();
		return this;
	
		
		
		
		
	}
	
	
	@FindBy(id="createLeadForm_lastName")
	private WebElement eleLasttName;
	public CreateLead typeLastname(String data) {
		//WebElement eleUsername = locateElement("id", "username");
		type(eleLasttName, data);
		//LoginPage lp = new LoginPage();
		return this;
	
				
	}
	
	
	@FindBy(id = "createLeadForm_dataSourceId")
	private WebElement elesource;
	public CreateLead sourcedropdown(int data) {
		selectDropDownUsingIndex(elesource, data);
		return this;
	}
	@FindBy(id="createLeadForm_marketingCampaignId")
	private WebElement eleCampaignId;
	public CreateLead clickSelect(int data) {
		selectDropDownUsingIndex(eleCampaignId,data);
		return this;
	
			
		
	}
	@FindBy(id = "createLeadForm_personalTitle")
	private WebElement eleper;
	public CreateLead typeper(String data) {
		type(eleper, data);
		return this;
	}
	
	@FindBy(id = "createLeadForm_generalProfTitle")
	private WebElement eleprof;
	public CreateLead typeprof(String data) {
		type(eleprof, data);
		return this;
	}
	@FindBy(id = "createLeadForm_annualRevenue")
	private WebElement eleannual;
	public CreateLead typeannual(String data) {
		type(eleannual, data);
		return this;
	}
	
	@FindBy(id = "createLeadForm_industryEnumId")
	private WebElement eleind;
	public CreateLead industry(String data) {
		selectDropDownUsingText(eleind, data);
		return this;
	}
	
	@FindBy(id = "createLeadForm_ownershipEnumId")
	private WebElement eleowner;
	public CreateLead owner(String data) {
		selectDropDownUsingText(eleowner, data);
		return this;
	}
	
	
	
	@FindBy(id = "createLeadForm_sicCode")
	private WebElement elesic;
	public CreateLead typesic(String data) {
		type(elesic, data);
		return this;
	}
	@FindBy(id = "createLeadForm_description")
	private WebElement eledescription;
	public CreateLead typedes(String data) {
		type(eledescription, data);
		return this;
	}
	
	@FindBy(id = "createLeadForm_importantNote")
	private WebElement elenote;
	public CreateLead typenote(String data) {
		type(elenote, data);
		return this;
	}
	
	@FindBy(id = "createLeadForm_lastNameLocal")
	private WebElement elelocal;
	public CreateLead typelocal(String data) {
		type(elelocal, data);
		return this;
	}
	@FindBy(id = "createLeadForm_departmentName")
	private WebElement eledep;
	public CreateLead typedepartment(String data) {
		type(eledep, data);
		return this;
	}
	@FindBy(id = "createLeadForm_numberEmployees")
	private WebElement eleemployee;
	public CreateLead typeemp(String data) {
		type(eleemployee, data);
		return this;
	}
	@FindBy(id = "createLeadForm_tickerSymbol")
	private WebElement eleticket;
	public CreateLead typeticket(String data) {
		type(eleticket, data);
		return this;
	}
	@FindBy(id = "createLeadForm_primaryPhoneCountryCode")
	private WebElement elecountry;
	public CreateLead typecoountry(String data) {
		type(elecountry, data);
		return this;
	}
	@FindBy(id = "createLeadForm_primaryPhoneAreaCode")
	private WebElement elearea;
	public CreateLead typearea(String data) {
		type(elearea, data);
		return this;
	}
	@FindBy(id = "createLeadForm_primaryPhoneExtension")
	private WebElement eleext;
	public CreateLead typeext(String data) {
		type(eleext, data);
		return this;
	}
	@FindBy(id = "createLeadForm_primaryEmail")
	private WebElement eleemail;
	public CreateLead typeemail(String data) {
		type(eleemail, data);
		return this;
	}
	@FindBy(id = "createLeadForm_primaryPhoneNumber")
	private WebElement elenum;
	public CreateLead typenum(String data) {
		type(elenum, data);
		return this;
	}
	@FindBy(id = "createLeadForm_primaryPhoneAskForName")
	private WebElement eleask;
	public CreateLead typeask(String data) {
		type(eleask, data);
		return this;
	}
	
	@FindBy(id = "createLeadForm_primaryWebUrl")
	private WebElement eleurl;
	public CreateLead typeurl(String data) {
		type(eleurl, data);
		return this;
	}
	
	@FindBy(className = "smallSubmit")
	private WebElement eleSub;
	public ViewLead clickcreate() {
		click(eleSub);
	return new ViewLead();
	}
	
	
	
	

	}
	
